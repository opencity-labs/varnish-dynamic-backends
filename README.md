# varnish-dynamic-backends

Dynamically add and remove Varnish backends

Inspired by https://github.com/tazjin/varnish-elb-systemd


## How to run manually

Add this in your varnish `default.vcl`

```
include "dynamic_backends.vcl";
```

then execute

```
update_varnish_backends NAME-OF-AWS-AUTO-SCALING-GROUP
```

you will then have `/etc/varnish/dynamic_backends.vcl` with a defined director with a backend for each IP
in the Autoscaling group instance, e.g.

```
# cat /etc/varnish/dynamic_backends.vcl
// my dynamic backends
//
backend instance1 { .host = "10.200.0.124"; }
backend instance2 { .host = "10.200.0.81"; }
backend instance3 { .host = "10.200.0.91"; }
sub vcl_init {
    new mydirector = directors.round_robin();
    mydirector.add_backend(instance3);
    mydirector.add_backend(instance2);
    mydirector.add_backend(instance1);
}
```

## Use in systemd

There are two pairs of related units included. To make this setup work two
things need to be done:

* regenerating the Varnish configuration periodically
* reloading Varnish if the configuration changes

The two unit pairs tackle these tasks separately.

`varnish-dynamic-backends.{timer|service}` provide a pair that will periodically run the
script. This is configured to run every 30 seconds.

`varnish-reload[@].{path|service}` provides a path unit that watches the specified
VCL file (`/etc/varnish/dynamic_backends.vcl`) and triggers the reload unit if the
VCL changes: after the reload unused backends are removed.

## Setup in systemd

(All the task below are available in `ansible_task.yml`)

* Create environment file in `/etc/default/varnish-dynamic-backends` with
```
# /etc/default/varnish-dynamic-backends
# see https://gitlab.com/opencity-labs/varnish-dynamic-backends
AWS_AUTO_SCALING_GROUP_NAME=<the name of your autoscaling group>
```
* Place scripts in `/usr/local/bin`
```
cp update_varnish_backends /usr/local/bin
cp remove_varnish_non_active_backends /usr/local/bin
```
* Place the units in `/usr/lib/systemd/system`
```
cp varnish-* /usr/lib/systemd/system
```
* Start the timer unit
```
systemctl daemon-reload
systemctl start varnish-dynamic-backends.timer
systemctl enable varnish-reload.{path,service}
systemctl start varnish-reload.{path,service}
```

### Debug and logs

```
# check execution (every 30 seconds)
journalctl -f

# this should return only one backend (and active)
varnishadm vcl.list
```

Test it by wiping out vcl file

```
echo "" > /etc/varnish/dynamic_backends.vcl
```

then in less then 30seconds you should see in
```
journalctl  -f
systemd[1]: Starting Generate Varnish backends...
Records changed, updating VCL file: /etc/varnish/dynamic_backends.vcl
```

#### Requirements

* varnish v.4.1
* AWS varnish instances, then one or more backend instance (nginx, apache.... ) that are part of an Autoscaling group
```
[default]
region = eu-west-1
```
* Tested with Centos 7
* aws cli setup, with in `/root/.aws/config` the aws region

* To be able to query the Autoscaling group to know the IP of all its instances you need to create the following policy and attach it to the Role of your vanish Ec2 instances
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowDescribeAutoScalingInstances",
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingInstances"
            ],
            "Resource": "*"
        },
        {
            "Sid": "AllowDescribeInstances",
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeInstances"
            ],
            "Resource": "*"
        }
    ]
}
```
